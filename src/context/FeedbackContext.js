import { createContext, useState, useEffect } from "react";
import { v4 as uuidv4 } from "uuid";
const FeedbackContext = createContext();

export const FeedbackProvider = ({ children }) => {
  const [isLoading, setIsLoading] = useState(true);

  const [feedback, setFeedback] = useState([]);

  useEffect(() => {    
    
      fetchFeedback();
   
  }, [])

  const fetchFeedback = async() => {
    const response  = await fetch(`/feedback?_sort=id&_order=desc`);
    const data = await response.json();
    setFeedback(data);
    setIsLoading(false);
    //console.log(data);

  }

  const [feedbackEdit, setFeedbackEdit] = useState({
    item: {},
    edit: false,
  });

  const deleteFeedback = async(id) => {
    if (window.confirm("Are you sure want ot delete")) {
      await fetch(`/feedback/${id}`,{
        method: "DELETE"
      })
      setFeedback(feedback.filter((item) => item.id !== id));
    }
  };

  const addFeedback = async(newFeedback) => {
    const response = await fetch("/feedback",{
      method: "POST",
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(newFeedback)
    });

    const data = await response.json();
    newFeedback.id = uuidv4();
    setFeedback([data, ...feedback]);
  };

  // set item to updated
  const editFeedback = (item) => {
    setFeedbackEdit({
      item,
      edit: true,
    });
  };

  const updateFeedback = async(id, updItem) => {
    const response = await fetch(`/feedback/${id}`,{
      method: "PUT",
      body: JSON.stringify(updItem),
      headers: {'Content-Type': 'application/json'},

    });
    const data = await response.json();
    setFeedback(
      feedback.map((item) =>  item.id === id ? {...item, ...data} : item)
    )
    
  };

  return (
    <FeedbackContext.Provider
      value={{
        feedback,
        isLoading,
        deleteFeedback,
        addFeedback,
        editFeedback,
        feedbackEdit,
        updateFeedback,
      }}
    >
      {children}
    </FeedbackContext.Provider>
  );
};

export default FeedbackContext;
