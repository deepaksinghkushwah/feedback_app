const FeedbackData = [
    {
        id: 1,
        rating: 3,
        text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptates excepturi a atque neque, ipsam in iure accusamus! Ducimus amet suscipit veritatis ex animi rerum doloremque quos praesentium tempora. Quaerat, totam.'
    },
    {
        id: 2,
        rating: 7,
        text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptates excepturi a atque neque, ipsam in iure accusamus! Ducimus amet suscipit veritatis ex animi rerum doloremque quos praesentium tempora. Quaerat, totam.'
    },
    {
        id: 3,
        rating: 6,
        text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptates excepturi a atque neque, ipsam in iure accusamus! Ducimus amet suscipit veritatis ex animi rerum doloremque quos praesentium tempora. Quaerat, totam.'
    },
    {
        id: 4,
        rating: 4,
        text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptates excepturi a atque neque, ipsam in iure accusamus! Ducimus amet suscipit veritatis ex animi rerum doloremque quos praesentium tempora. Quaerat, totam.'
    },
    {
        id: 5,
        rating: 10,
        text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptates excepturi a atque neque, ipsam in iure accusamus! Ducimus amet suscipit veritatis ex animi rerum doloremque quos praesentium tempora. Quaerat, totam.'
    }

];

export default FeedbackData;