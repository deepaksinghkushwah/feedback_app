import {useParams} from 'react-router-dom'
function Post() {
    const params = useParams()
    return (
        <div>
            <h1>Post</h1>
            <p>ID: {params.id}</p>
            <p>Name; {params.name}</p>
        </div>
    )
}

export default Post
