import {useState, useContext, useEffect} from 'react'
import FeedbackContext from '../context/FeedbackContext';

function RatingSelect({select}) {
    const [selected, setSelected] = useState(10);
    const {feedbackEdit} = useContext(FeedbackContext);

    useEffect(() =>     {
        setSelected(feedbackEdit.item.rating);
    }, [feedbackEdit])
    const ratingArr = [1,2,3,4,5,6,7,8,9,10];
    const handleChange = (e) => {
        // + sign convert it to number
        setSelected(+e.currentTarget.value); 
        select(+e.currentTarget.value);        
    };
    return (
        <ul className='rating'>
            {ratingArr.map((item,index) => (
                <li key={index}>
                <input type="radio" name="rating" id={`num${item}`} value={item} onChange={handleChange} checked={selected === item} />
                <label htmlFor={`num${item}`}>{item}</label>
            </li>
            ))}
            
            
            
        </ul>
    )
}

RatingSelect.propTypes = {

}

export default RatingSelect
