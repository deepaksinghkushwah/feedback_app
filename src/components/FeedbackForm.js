import {useState, useEffect} from 'react'
import Card from './shared/Card'
import Button from './shared/Button';
import RatingSelect from './RatingSelect';
import FeedbackContext from '../context/FeedbackContext';
import {useContext} from "react"
function FeedbackForm() {
    const {addFeedback, feedbackEdit, updateFeedback} = useContext(FeedbackContext);
    useEffect(() => {
        console.log("hekki");
        if(feedbackEdit.edit === true){
            setBtndisabled(false);
            setText(feedbackEdit.item.text);
            setRating(feedbackEdit.item.rating);

        }
    },[feedbackEdit]);

    const [text, setText] = useState('');
    const [btnDisabled, setBtndisabled] = useState(true);
    const [message, setMessage] = useState("");
    const [rating, setRating] = useState(10);
    const handleTextChange = (e) => {        
        setText(e.target.value);
        if(text !== '' && text.trim().length > 10){
            setBtndisabled(false);
            setMessage("");
        } else if(text === '') {
            setBtndisabled(true);
            setMessage("");
        } else {
            setBtndisabled(true);
            setMessage("Review length must be min 10 char long");
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if(text.trim().length > 10){
            const newFeedback = {
                text,
                rating
            };
            if(feedbackEdit.edit === true){
                updateFeedback(feedbackEdit.item.id, newFeedback);
            } else {
                addFeedback(newFeedback);
            }
            
            setText("");
        }
    }
    return (
        <Card>
            <form onSubmit={handleSubmit}>
                <h2>How would you rate your service with us?</h2>
                <RatingSelect select={(rating) => setRating(rating)}/>
                <div className="input-group">
                    <input onChange={handleTextChange} type="text" placeholder='Write a review' value={text} />
                    <Button isDisabled={btnDisabled} type="submit">Send</Button>
                </div>
                {message && <div className="message">{message}</div>}

            </form>
        </Card>
    )
}

FeedbackForm.propTypes = {

}

export default FeedbackForm

