import Card from "../components/shared/Card"
import { Link } from "react-router-dom";
function AboutPage() {
    return (
        <Card>
            <h1>About</h1>
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aliquid non hic nulla sint quasi quis sunt eum illo ut, id esse ab sed suscipit magni delectus itaque deleniti at soluta?</p>
            <Link to="/">Back to home</Link>
        </Card>
    )
}

export default AboutPage